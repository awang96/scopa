# Notes

Etat : les 40 cartes
* 0 -> cartes non jouées
* 1 -> cartes en main
* 2 -> cartes sur le board
* 3 -> cartes jouées

Action : jouer une carte

Valeur des cartes :
* 7 -> 21
* 6 -> 18
* 1 -> 16
* 5 -> 15
* 4 -> 14
* 3 -> 13
* 2 -> 12
* 8, 9, 10 -> 10

Valeur d'une carte : Valeur du chiffre + 10 pour or + 10 pour 7 or

Récompenses :
* action impossible -> -100
* carte jouée ->
    * aucune carte achetée -> - valeur de la carte - 20
    * cartes achetées -> somme des valeurs des cartes achetées
    * reste - de 10 sur le board -> -10
    * scopa -> 60
  
* le plus de carte : 10
* le plus d'or : 10
* la première : 10